events.api package
==================

.. automodule:: events.api
   :members:
   :undoc-members:
   :show-inheritance:

Submodules
----------

events.api.permissions module
-----------------------------

.. automodule:: events.api.permissions
   :members:
   :undoc-members:
   :show-inheritance:

events.api.serializers module
-----------------------------

.. automodule:: events.api.serializers
   :members:
   :undoc-members:
   :show-inheritance:

events.api.urls module
----------------------

.. automodule:: events.api.urls
   :members:
   :undoc-members:
   :show-inheritance:

events.api.viewsets module
--------------------------

.. automodule:: events.api.viewsets
   :members:
   :undoc-members:
   :show-inheritance:

