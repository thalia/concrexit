��    �      �  �         �     �  #   �  +   �               &     .     F     N     ]  
   a     l     x  
   �     �     �     �  1   �     �       (        =     D     V  A   ^  .   �  U  �    %  �  2  �  �  �  �    �  E  �     �      �   )   �   +   &!  0   R!     �!     �!     �!     �!  N   �!     "     )"  #   H"  ,   l"  '   �"  �   �"     �#  
   �#  
   �#     �#     �#     �#     �#  %   
$  2   0$     c$     ~$     �$     �$     �$     �$     �$  	   �$  	   �$  )   %     -%     4%     <%     T%  "   q%  '   �%     �%  !   �%  -   �%     )&     :&     J&     [&  $   x&     �&     �&     �&     �&     �&     �&     �&     �&     '  %   
'      0'  !   Q'  !   s'      �'     �'     �'  '   �'     �'     (  C   (     ](     p(     �(     �(     �(     �(     �(     �(  $   �(     $)     @)     U)  U   h)  	   �)     �)  
   �)     �)     �)     *      *     9*  f   Y*  4   �*  2   �*  *   (+  f   S+  &   �+  .   �+  .   ,     ?,     ^,     f,     �,     �,     �,     �,  �   �,     j-  '   r-  #   �-  #   �-     �-  ,   �-     (.     ,.  6   F.  #   }.  +   �.  9   �.     /     /     /  	   +/  
   5/     @/     X/     e/     k/     r/     �/     �/     �/  '   �/     �/     �/     �/     �/  !   0     40     :0     C0     Q0  m  X0     �1  .   �1  0   �1     +2     02     P2     X2     p2     v2     �2     �2  
   �2  "   �2     �2     �2  
   �2  
   �2  5   �2  %   53  
   [3  *   f3     �3     �3     �3  R   �3  7   4  �  H4  ^  �5  �  <:  �  >  �  �?    �A    �B     �D     �D  2   �D  5   &E  5   \E  '   �E     �E     �E     �E  Y   �E     3F     MF  3   hF  7   �F  ,   �F  �   G  )   �G     �G     �G  	   H     H  
   H     'H  *   /H  5   ZH     �H     �H  	   �H     �H  &   �H  	   �H  !   �H  
   I  
   *I  /   5I     eI     iI     oI  &   �I     �I  *   �I     �I  (   J  /   <J  	   lJ     vJ     ~J     �J  4   �J     �J     �J     �J  #   �J     K     K     K     (K     7K      <K  *   ]K     �K  %   �K     �K     �K     �K  '   L     +L     ;L  E   HL     �L     �L     �L     �L     �L     �L     M     M  "   5M     XM     xM     �M  T   �M     �M  	   �M     N     N     &N     4N     ;N  *   ZN  f   �N  5   �N  4   "O  $   WO  k   |O  +   �O  4   P  /   IP     yP     �P  #   �P  	   �P     �P     �P  #   Q  �   )Q     �Q  "   �Q  "   �Q  &   R     )R  ,   6R     cR     fR  L   R     �R  #   �R  7   S     DS     MS     dS  	   xS  
   �S     �S     �S     �S  	   �S     �S     �S     �S     �S  !   �S     T      T     (T  &   FT  %   mT     �T     �T     �T     �T     A   �       R       W   k             �   [       )   s       7   1   a   �   `      5   0   B   ]   �       �         X   �   :   4   f   �   b       m   |               �   @   T       �   N   &   �   r   Y   p          I   �   �              g   (   �   "   x   9   2             V          -   q   {   �   j   �   \       �           J   �   ^   o   Q   H         �   <          �   >   +   S   C               E      �   �   �   �       �                   ~   �   �      �       *   
      G   3   	             �           �           l   h   �   c   d       '   ?   e             u   y   L      =   F       �           n   �       U   �   �   %   �   i   t   �   �         w      �           �   8       6   �   ;   M   Z      $       �   #   /          K       �           !   �   P       v   �   �       }       �                 �               ,   �      z   O       .       D   _        < 18 A birthday cannot be in the future. A membership already exists for that period About Access NextCloud as admin Account Achievements for Thalia Address Address line 2 Age All events All members Automatically renew membership Benefactor Benefactors Birthday Chair Change the financial information known to Thalia. Change your accounts' password. City Click here to change your email address. Cohort Computing Science Country Data minimisation could not be executed for the selected user(s). Data minimisation was executed for {} user(s). Dear %(full_name)s,

Welcome to Study Association Thalia! You now have an account and can
log in at %(url)s.

Your username is:   %(username)s
Your password is:   %(password)s

Please also check the information on your profile.

With kind regards,

The board of Study Association Thalia

————

This email was automatically generated. Dear %(name)s,

The past year you've been a member or benificiary of Study Association Thalia.
You've not given us permission to prolong it automatically, thus your membership will
end on the 1st of September.

If you still want to enjoy all the benefits a membership gives you the coming year
then you'll have to renew it. This'll give you access to activities, the book sale,
our exam collection and a symposium.

Renewing your membership is easy. Open the renewal page on the website and
select the right option: %(renewal_url)s.

Note that if you upgrade your yearly membership to a study membership before August 31,
you will get a discount of €%(membership_price)s.

Are you unable to renew your membership via the website? Then send us an email: info@thalia.nu.

With kind regards,

The board of Study Association Thalia

————

This email was automatically generated. It's possible that you've already renewed
your membership and that it hasn't been processed yet. If that's the case then
you don't need to do anything. Dear %(name)s,

We currently have the following information about you in our database.
Via this email we'd like to ask you to check the accuracy of this data.

Username:               %(username)s
Name:                   %(full_name)s
Address:                %(address_street)s
                        %(address_street2)s
                        %(address_postal_code)s
                        %(address_city)s
                        %(address_country)s
Phone:                  %(phone_number)s
Date of birth:          %(birthday)s
Email address:          %(email)s
Student number:         %(student_number)s
Cohort:                 %(starting_year)s
Programme:              %(programme)s

You can change this data by logging into the website and using the 'edit profile' page.
Unable to edit the incorrect information? Then send an email: info@thalia.nu.

With kind regards,

The board of Study Association Thalia

————

This email was automatically generated. Dear %(name)s,

We've received a request to change the email address of your account.
If you did not submit such a request then please ignore this email.

Please open the following link in your web browser to confirm the change:
%(confirm_link)s

If you have any questions, then don't hesitate and send an email to info@thalia.nu.

With kind regards,

The board of Study Association Thalia

————

This email was automatically generated. Dear %(name)s,

We've received a request to change the email address of your account.
If you did not submit such a request then please ignore this email.

Please open the following link in your web browser to confirm your new email address:
%(confirm_link)s

If you have any questions, then don't hesitate and send an email to info@thalia.nu.

With kind regards,

The board of Study Association Thalia

————

This email was automatically generated. Dear %(name)s,

We've successfully updated the email address of your account.

If you have any questions, then don't hesitate and send an email to info@thalia.nu.

With kind regards,

The board of Study Association Thalia

————

This email was automatically generated. Dear %(name)s,

You're a study member of Thalia. This means that you'll be a member as long
as you're enrolled in either a Computing Science or an Information Sciences programme.

Are you still studying? Then you don't have to do anything at all!
Did you graduate or stop with your studies for any other reason? Then please
send us an email so that we can update our administration: info@thalia.nu

If you have any questions, then don't hesitate and send us an email.

With kind regards,

The board of Study Association Thalia

————

This email was automatically generated. Display birthday Display name Download address label for selected users Download email addresses for selected users Download student number label for selected users Edit your profile and avatar. Email Email Address Email address Email change request for {} to {} created at {} (confirmed: {}, verified: {}). Emergency contact name Emergency contact phone number End date can't be before start date Enter a phone number so Thalia may reach you Enter a valid student- or e/z/u-number. Ever wondered what the name of that person in the back row of the lecture room is? There is a high probability that this person is a member of Thalia, and thus you can use this directory to find him or her. Export IBANs for Direct Debit First Name First name Former Members Former benefactor Former honorary member Former member Get an overview of all your expenses. Get information about your membership or renew it. Has individual permissions Honorary Member Honorary Members How to display name Information Sciences Initials Is this user currently active Last Name Last name Leave the restricted area of the website. Member Members Membership announcement Membership announcement sent Membership expiration announcement Membership expiration announcement sent Membership information check Membership information check sent Membership of type {} for {} ({}) starting {} Membership since Membership type Membership until Membership until x until {} Minimise data for the selected users Nickname No No members found No, manual renewal required. None Older Personal information Phone number Photo Please check your profile for errors. Please confirm your email change Please enter a new email address. Please enter a valid phone number Please verify your email address Postal code Preferred language Preferred language for e.g. newsletters Profile for {} Profile text Receive mailings about vacancies and events from Thalia's partners. Receive newsletter Receive opt-in mailings Receive the Thalia Newsletter Search Second address line Send welcome email Show full name Show initials and last name Show name like "John 'nickname' Doe" Show nickname and last name Show only first name Show only nickname Show your birthday to other members on your profile page and in the birthday calendar Societies Starting year Statistics Street and house number Student number Study programme Take a look at your own profile. Text to display on your profile The confirmation of your email change was successful. Check your new inbox for the verification email. The date the member started holding this membership. The date the member stops holding this membership. The phone number for the emergency contact The verification of your email change was successful. Check your old inbox for the confirmation email. The year this member started studying. This email will include the generated password This member has not written a description yet. Total amount of Thalia members Unknown Unknown membership history User User may not attend anything User may not attend drinks User may not attend events We have received the request to change your email address. You should receive a message in both your new and old mailboxes to verify the change. Website Website to display on your profile page Welcome to Study Association Thalia Which events can this member attend Who are you looking for? Who should we contact in case of emergencies Yes Yes, enable auto renewal. You need to enter a nickname to use it as display name Your email address has been changed Your profile has been updated successfully. You’re currently logged in as <strong>%(user)s</strong> change change email change password confirmed created at current membership type edit profile email logout manage bank account(s) manage membership member members please use the format <street> <number> profile save show public profile the new email address is valid the old email address was checked today verified view payments ≥ 18 Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2019-09-08 10:57+0200
PO-Revision-Date: 2019-09-08 10:57+0200
Last-Translator: Thom Wiggers <thom@thomwiggers.nl>
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2.3
 < 18 Een verjaardag kan niet in de toekomst liggen. Er bestaat al een lidmaatschap voor deze periode Over Toegang tot NextCloud als admin Account Verdiensten voor Thalia Adres Tweede adresregel Leeftijd Alle evenementen Alle leden Lidmaatschap automatisch verlengen Begunstiger Begunstigers Verjaardag Voorzitter Verander de financiële informatie bekend bij Thalia. Wijzig het wachtwoord van je account. Woonplaats Klik hier om je e-mailadres aan te passen. Cohort Informatica (Computing Science) Land Gegevensminimisatie kon niet worden uitgevoerd voor de geselecteerde gebruiker(s). Gegevensminimisatie is uitgevoerd voor {} gebruiker(s). Beste %(full_name)s,

Welkom bij Studievereniging Thalia! Je hebt nu een account zodat 
je kunt inloggen op %(url)s.

Dit zijn je gebruikersnaam en wachtwoord: 
Je gebruikersnaam is:   %(username)s
Je wachtwoord:   %(password)s


Controleer alsjeblieft ook de gegevens in je profiel. 

Met vriendelijke groet,
Het bestuur der Studievereniging Thalia

————

Deze e-mail is automatisch gegenereerd. Beste %(name)s,

Het afgelopen collegejaar ben je lid of begunstiger geweest van Studievereniging
Thalia. Je hebt daarbij geen doorlopende machtiging afgegeven, dus je
lidmaatschap loopt op 1 september af.

Wil je volgend jaar ook nog van de voordelen van een Thalialidmaatschap
genieten, zoals de borrels, boekverkoop, activiteiten, de tentamenbundel, de
Thabloid en een symposium, dan zul je je lidmaatschap moeten verlengen.

Het vernieuwen van je lidmaatschap is makkelijk. Open de vernieuwingspagina op de website en
selecteer de juiste optie: %(renewal_url)s.

Als je je jaarlidmaatschap voor 31 augustus nog omzet naar een studielidmaatschap krijg je
daarvoor €%(membership_price)s korting.

Ook op een later moment kun je je nog inschrijven, neem dan even contact op met
het bestuur om te bespreken wat een handig moment is. Mail hiervoor naar
info@thalia.nu.

Met vriendelijke groeten,

Het bestuur der Studievereniging Thalia


————

Deze e-mail is automatisch gegenereerd. Het kan zijn dat je je lidmaatschap al
verlengd hebt, maar dat dit nog niet is verwerkt: in dat geval hoef je niets te doen. Beste %(name)s,

Hieronder vind je de (meeste) gegevens die we op dit moment van je geregistreerd
hebben. Wil je deze controleren op juistheid?

Gebruikersnaam:         %(username)s
Naam:                   %(full_name)s
Adres:                  %(address_street)s
                        %(address_street2)s
                        %(address_postal_code)s
                        %(address_city)s

                        %(address_country)s
Telefoonnummer:         %(phone_number)s
Geboortedatum:          %(birthday)s
Emailadres:             %(email)s
Studentnummer:          %(student_number)s
Cohort:                 %(starting_year)s
Programma:              %(programme)s

Je kunt als je ingelogd bent via ‘Mijn gegevens’ de meeste van deze gegevens
aanpassen. Klopt iets niet wat je niet zelf kunt bewerken? Stuur dan een mailtje
naar info@thalia.nu.

Met vriendelijke groet,
Het bestuur der Studievereniging Thalia

————

Deze e-mail is automatisch gegenereerd. Beste %(name)s,

We hebben een verzoek ontvangen om het e-mailadres van je account aan te passen.
Mocht je dit verzoek niet zelf hebben ingediend, negeer deze e-mail dan.

Open de volgende link in je webbrowser om de verandering te bevestigen:
%(confirm_link)s

Mocht je vragen hebben, stuur dan vooral een mailtje naar info@thalia.nu.

Met vriendelijke groet,
Het bestuur der Studievereniging Thalia

————

Deze e-mail is automatisch gegenereerd. Beste %(name)s,

We hebben een verzoek ontvangen om het e-mailadres van je account aan te passen.
Mocht je dit verzoek niet zelf hebben ingediend, negeer deze e-mail dan.

Open de volgende link in je webbrowser om je nieuwe e-mailadres te verifiëren:
%(confirm_link)s

Mocht je vragen hebben, stuur dan vooral een mailtje naar info@thalia.nu.

Met vriendelijke groet,
Het bestuur der Studievereniging Thalia

————

Deze e-mail is automatisch gegenereerd. Beste %(name)s,

We hebben het e-mailadres van je account succesvol bijgewerkt.

Mocht je vragen hebben, stuur dan vooral een mailtje naar info@thalia.nu.

Met vriendelijke groet,
Het bestuur der Studievereniging Thalia

————

Deze e-mail is automatisch gegenereerd. Beste %(name)s,

Je bent studielid van Thalia. Dit betekent dat zolang je informatica of
informatiekunde studeert je lid bent van Thalia. Studeer je nog? Dan hoef je
niets te doen! Ben je afgestudeerd of om een andere reden gestopt met je studie?
Stuur dan even een mailtje naar info@thalia.nu, zodat we je uit het systeem
kunnen halen.

Als je vragen hebt over dit mailje kun je uiteraard ook contact opnemen.

Met vriendelijke groeten,

Het bestuur der Studievereniging Thalia

————

Deze e-mail is automatisch gegenereerd. Laat verjaardag zien Weergavenaam Download adreslabels voor geselecteerde gebruikers Download e-mailadressen voor geselecteerde gebruikers Download studentnummers voor geselecteerde gebruikers Bewerk je profiel en profielafbeelding. E-mail E-mailadres E-mailadres E-mail wijzigingsverzoek voor {} naar {} gemaakt op {} (bevestigd: {}, geverifieerd: {}). Contact voor noodgevallen Telefoonnummer noodcontact De einddatum kan niet eerder zijn dan de startdatum Voer een telefoonnummer in zodat Thalia je kan bereiken Voer een geldig student- of e/z/u-nummer in. Wel eens op zoek naar de naam van die persoon die je altijd in de gangen ziet lopen? Er bestaat een grote kans dat die persoon lid van Thalia is, en dus is opgenomen in de onderstaande ledenlijst. Exporteer IBANs voor automatische incasso Voornaam Voornaam Oud-leden Oud-begunstiger Oud-erelid Oud-lid Verkrijg een overzicht van al je uitgaven. Bekijk je lidmaatschap of vernieuw deze indien nodig. Heeft individuele permissies Erelid Ere-leden Weergave naam Informatiekunde (Information Sciences) Initialen Is deze user op dit moment actief Achternaam Achternaam Verlaat het beveiligde gedeelte van de website. Lid Leden Mededeling over lidmaatschap Mededeling over lidmaatschap verzonden Verlopen lidmaatschap Meldingen vervallen lidmaatschap verzonden Controle gegevens lidmaatschap Controle gegevens lidmaatschap verzonden Lidmaatschap van type {} van {} ({}) gestart {} Lid sinds Lidtype Lid tot  tot {} Minimiseer gegevens voor de geselecteerde gebruikers Bijnaam Nee Geen leden gevonden Nee, handmatige verlenging vereist. Geen Ouder Persoonlijke gegevens Telefoonnummer Foto Controleer je profiel op fouten. Bevestig de verandering van je e-mailadres Vul een nieuw e-mailadres in. Voer svp een geldig telefoonnummer in Verifieer je nieuwe e-mailadres Postcode Voorkeurstaal Voorkeurstaal voor b.v.b. nieuwsbrieven Profiel voor {} Profieltekst Ontvang mailings over vacatures en evenementen van Thalia's partners. Ontvang nieuwsbrief Ontvang opt-in mailings Ontvang de Thalia nieuwsbrief Zoeken Tweede adresregel Stuur welkomste-mails Volledige naam Alleen initialen en achternaam Laat zien als "John 'bijnaam' Doe" Laat bijnaam en achternaam zien Alleen voornaam Alleen bijnaam Toon je verjaardag aan andere leden op je profielpagina en in de verjaardagskalender Gezelschappen Startjaar Statistieken Straat en huisnummer Studentnummer Studie Bekijk je eigen profielpagina. Tekst om te laten zien op je profielpagina De verandering van je e-mailadres is bevestigd. Controleer je nieuwe inbox voor de verificatie e-mail. De datum waarop het lid dit lidmaatschap is begonnen. De datum waarop het lid dit lidmaatschap beëindigd. Het telefoonummer van de noodcontact Het nieuwe e-mailadres van je account is geverifieerd. Controleer je oude inbox voor de bevestigingse-mail. Het jaar waarop dit lid begon met studeren. Deze e-mail zal het gegenereerde wachtwoord bevatten Dit lid heeft nog geen beschrijving geschreven. Totaal aantal Thalialeden Onbekend Onbekende lidmaatschap geschiedenis Gebruiker Gebruiker mag nergens heen Gebruiker mag niet naar borrels Gebruiker mag niet naar evenementen We hebben je verzoek tot verandering van je e-mailadres ontvangen. Controleer je oude en nieuwe mailboxen om de verandering te bevestigen. Website Website om op je profiel te linken Welkom bij Studievereniging Thalia Welke evenementen mag dit lid bijwonen Wie zoek je? Wie Thalia moet bereiken in bij noodgevallen Ja Ja, verleng automatisch. Je moet een bijnaam invoeren voordat je deze kunt gebruiken als weergavenaam Je e-mailadres is aangepast Je profiel is succesvol opgeslagen. U bent momenteel ingelogd als <strong>%(user)s</strong> verander e-mailadres veranderen wachtwoord wijzigen bevestigd gemaakt op huidig lidtype profiel bewerken e-mail uitloggen bankrekening(en) beheren lidmaatschap beheren lid leden specificeer als <straat> <nummer> profiel opslaan bekijk publieke profielpagina het nieuwe e-mailadres is geverifieerd het oude e-mailadres is gecontroleerd heden geverifieerd bekijk betalingen ≥ 18 