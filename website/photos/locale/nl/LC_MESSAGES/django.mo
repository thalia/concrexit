��          �      �        :   	     D  ~   P     �  f   �     F     M  +   T  -   �  @   �     �     	                     /     4     ;  	   D     N     S     Y  w  j  M   �     0  �   C     �  Z   �     4     ;  6   B  )   y  e   �     	     #     )     9     ?     G  	   O     Y     a     j     o     u                                                                
   	                                     Full-sized photos will not be saved on the Thalia-website. Ignoring {} Interested in a full resolution version of a photo? Send your request to <a href="mailto:media@thalia.nu">media@thalia.nu</a>. No albums found Note: This album can be shared with people outside the association by sending them the following link: Photos Search The uploaded file is not a zip or tar file. This does not modify the original image file. Uploading a zip or tar file adds all contained images as photos. What are you looking for? album cover image date directory name file hidden rotation shareable slug title {} is duplicate. Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2018-11-05 21:01+0100
PO-Revision-Date: 2018-11-05 21:02+0100
Last-Translator: Sébastiaan Versteeg <se_bastiaan@outlook.com>
Language-Team: 
Language: nl
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 2.2
 De foto’s op de Thalia-website worden niet opgeslagen in originele grootte. {} wordt genegeerd Wil je graag een foto in de volledige resolutie? Stuur je verzoek naar <a href=“mailto:media@thalia.nu”>media@thalia.nu</a>. Geen albums gevonden Let op: Dit album kan gedeeld worden met mensen buiten de vereniging via de volgende link: Foto's Zoeken Het geüploade bestand is geen als zip of tar archief. Dit verandert het originele bestand niet. Indien je een zip of tar file upload worden alle afbeeldingen in het bestand als foto’s toegevoegd. Waar ben je naar op zoek? album coverafbeelding datum mapnaam bestand verborgen rotatie deelbaar slug titel {} is dubbel. 