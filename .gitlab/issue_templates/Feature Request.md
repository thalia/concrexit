/label ~feature ~"priority: low"

<!--
    You want something new.
-->

### One-sentence description

<!-- What do you want? -->

### Motivation

<!-- Why should we care? -->

### Desired functionality

<!--
    What we should support.
    Please provide this in general terms, if possible.
    You can provide suggestions on how to implement this later.

    We like high-level descriptions as those allow us to better consider
    the desired result. Assumptions on how the site works don't
    necessarily match how we implemented certain things.
-->

### Suggested implementation

<!--
    If you have any notes on how we could achieve this feature,
    share them here.
-->
